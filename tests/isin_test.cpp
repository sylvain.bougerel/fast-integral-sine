#include <cmath>
#include <cstdint>
#include <iostream>
#include <x86intrin.h>

#include "../src/isin.hpp"

int main() {
  // Warm up
  {
    double s = 0;
    uint64_t start = __rdtsc();
    // even when empty, back-to-back __rdtsc() doesn't optimize away
    for (double a = -4 * M_PI; a < 4 * M_PI; a += M_PI / 180.0)
      s += std::sin(a);
    uint64_t elapsed = __rdtsc() - start;
    std::cout << "warm up std::sin():\t" << elapsed << " cycles (" << s << ")"
              << std::endl;
  }

  {
    int s = 0;
    uint64_t start = __rdtsc();
    for (int a = -4 * 180; a < 4 * 180; a += 1)
      s += isin(a, 2 << 10);
    uint64_t elapsed = __rdtsc() - start;
    std::cout << "warm up isin():\t" << elapsed << " cycles (" << s << ")"
              << std::endl;
  }

  {
    int s = 0;
    uint64_t start = __rdtsc();
    for (int a = -4 * 180; a < 4 * 180; a += 1)
      s += isin_lut(a);
    uint64_t elapsed = __rdtsc() - start;
    std::cout << "warm up isin_lut():\t" << elapsed << " cycles (" << s << ")"
              << std::endl;
  }

  // Actual
  {
    double s = 0;
    uint64_t start = __rdtsc();
    for (double a = -4 * M_PI; a < 4 * M_PI; a += M_PI / 180.0)
      s += std::sin(a);
    uint64_t elapsed = __rdtsc() - start;
    std::cout << "std::sin() for 1440 iterations:\t" << elapsed << " cycles\t"
              << elapsed / 1440 << " avg cycle per calculation (" << s << ")"
              << std::endl;
  }

  {
    int s = 0;
    uint64_t start = __rdtsc();
    for (int a = -4 * 180; a < 4 * 180; a += 1)
      s += isin(a, 2 << 15);
    uint64_t elapsed = __rdtsc() - start;
    std::cout << "isin() for 1440 iteration:\t" << elapsed << " cycles\t"
              << elapsed / 1440 << " avg cycle per calculation (" << s << ")"
              << std::endl;
  }

  {
    int s = 0;
    uint64_t start = __rdtsc();
    for (int a = -4 * 180; a < 4 * 180; a += 1)
      s += isin_lut(a);
    uint64_t elapsed = __rdtsc() - start;
    std::cout << "isin_lut() for 1440 iteration:\t" << elapsed << " cycles\t"
              << elapsed / 1440 << " avg cycle per calculation (" << s << ")"
              << std::endl;
  }

  return 0;
}
