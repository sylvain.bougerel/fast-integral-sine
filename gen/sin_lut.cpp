#include <cmath>
#include <format>
#include <iostream>

int main() {
  for (double a = 0; a < 180; ++a)
    std::cout << std::format("{}", std::sin(a * M_PI / 180.0)) << ", // " << a
              << std::endl;
}
