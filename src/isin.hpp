#ifndef ISIN_H_
#define ISIN_H_

#include "sin_lut.hpp"
#include <cmath>
#include <cstdint>

template <int P> struct SinTable180points {
  constexpr SinTable180points() : values() {
    for (auto i = 0; i < 180; ++i) {
      values[i] = sin_lut[i] * P;
    }
  }
  int values[180];
};

inline int isin(int angle, int scale) {
  const int slide = sizeof(int) * 8 - 1; // 31 for 32-bit ints
  int s = angle >> slide;                // sign mask: 0 or -1
  int aa = (angle ^ s) - s;              // absolute angle
  int Qh = aa / 90;                      // quotient quadrants
  int Qr = aa % 90;                      // reminder quadrants
  int q = (Qh & 1) ? 90 - Qr : Qr;       // first quadrant symmetry
  int h = -((Qh & 2) >> 1);              // semi-circle (half) mask: 0 or -1
  int64_t a = (q * scale);
  int64_t b = (3 * 90 * 90 - q * q);
  int64_t c = a * b / (2 * 90 * 90 * 90);
  int val = c;
  s = h ^ s;            // XOR half and sign mask together
  return (val ^ s) - s; // negate val, same as "val * s"
}

inline int isin_lut(int angle) {
  // Static table with precalculated outputs
  static constexpr const auto table = SinTable180points<2 << 15>();
  const int slide = sizeof(int) * 8 - 1; // 31 for 32-bit int
  int s = angle >> slide;                // sign mask: 0 or -1
  int aa = (angle ^ s) - s;              // absolute angle
  int h = aa / 180;                      // quotient semi-circle
  int ha = aa % 180;                     // reminder semi-circle
  h = -(h & 1);                          // semi-circle mask: 0 or -1
  s = h ^ s;                             // XOR semi-circle and sign together
  return (table.values[ha] ^ s) - s;
}

#endif // ISIN_H_
